﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows;
using System.IO;
using System.Diagnostics;

namespace LaunchGame
{
    public class PlayGame
    {
        public static void LaunchGame(string token)
        {
                 string strPlayCookie = "TTI_PLAYCOOKIE";
                 string strSetGameServer = "TTI_GAMESERVER";
                 string strIP = "167.114.28.238";
                 Environment.SetEnvironmentVariable(strPlayCookie, token);
                 Environment.SetEnvironmentVariable(strSetGameServer, strIP);
                 ProcessStartInfo startInfo = new ProcessStartInfo();
                 startInfo.FileName = "infinite.exe";
                 startInfo.Arguments = "-t " + token;
                 Process.Start(startInfo);
        }
  
        public static string getToken(string Username, string Password)
        {
            WebRequest request = WebRequest.Create("https://toontowninfinite.com/api/login/");
            request.Method = "POST";
            string postData = "n=" + Username + "&p=" + Password + "&dist=test";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            dynamic tok = (responseFromServer.Split(':')[2]).Replace("}", "").Replace("\"", "");
            return tok;
        }

    }
}
