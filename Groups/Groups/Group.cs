﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groups
{
    public class Group
    {

        private static String m_type;                               // Types consists of Building, CFO, CJ, CEO, VP, Golf and so on
        private static String m_hood;                               // Hood consist of all neighboor hoods included hq's                          
        private static int m_members_needed;                        // Members needed is how many members are needed to complete the group
        private static ArrayList m_member_toon;                     // Name of members currently in group
        private static String m_street;                             // The street if any
        private static String m_name;                               // Derp, the name of the group.
        private static String m_district;                           // District

        public static int ID;                                      /* ID is the most important. The ID is a PRIMARY KEY on the mysql database.
                                                                     * The ID is the only thing that makes your group unique.
                                                                     */

        public Group() { }

        public int getID()
        {
            return ID;
        }

        /// <summary>
        /// Group Constructor. Note after constructing there is no need to 
        /// </summary>
        /// <param name="name">Name of the group</param>
        /// <param name="type">Types consists of Building, CFO, CJ, CEO, VP, Golf and so on</param>
        /// <param name="hood">Hood consist of all neighboor hoods included hq's</param>
        /// <param name="street">Street name if any. You may enter this as null</param>
        /// <param name="members_needed">Members needed is how many members are needed to complete the group</param>
        /// <param name="member_toon">In an array the name of members currently in group</param>
        /// <param name="requirments">Requirments of toon to join the group</param>
        public Group(String type, String street, int members_needed, String district)
        {

            m_type = "";                                // Initialize type
            m_hood = "";                                // Initialize hood
            m_members_needed = 0;                       // Initialize m_members_needed
            m_member_toon = new ArrayList();            // Initialize m_member_toon
            m_street = "";                              // Initialize m_street
            m_name = "";                                // Initialize m_name
            m_district = "";                            // Initialize m_district

            m_type = type;                              // Set local type variable to usable m_type variable
            //m_hood = hood;                              // Set local hood variable to usable m_hood variable
            m_members_needed = members_needed;          // Set local members_needed variable to usable m_members_needed variable
            //m_member_toon = member_toon;                // Set local member_toon variable to usable m_member_toon variable 
            //m_name = name;                              // Set local name variable to usable m_name variable
            m_street = street;                          // Set local street variable to usable m_street variable 
            m_district = district;                      // Set local district variable to usable m_district variable

            publishGroup();                             // Extract GROUP ID from returned array list of publishGroup function.

        }

        /// <summary>
        /// Converts Toon array to csv
        /// </summary>
        /// <param name="toons">The array list of member toons to be converted to csv</param>
        private String memberToonsToCSV(ArrayList toons)
        {
            String data = "";                           // Create return variable of data
            foreach (String toon in toons)              // Loop through each toon                                                                    
            {
                data = data + toon + ",";               // Append data to the string
            }
            return data;                                // Return the csv value of toons
        }

        /// <summary>
        /// publishGroup publishes the group to the internet for users to view with application.
        /// </summary>
        /// <returns>
        /// Returns an array list like so. [Bool Success, int ID]
        /// </returns>
        private ArrayList publishGroup()
        {

            String createGroupURL = "http://launcher.zzl.org/createGroup.php?" +
                "type=" + m_type +                                                                                                                                        
                "&members_needed=" + m_members_needed.ToString() +                                                                        
                "&street=" + m_street +                                                                                                                              // Set name
                "&district=" + m_district;                                                              

            System.Net.WebClient wc = new System.Net.WebClient();
            byte[] raw = wc.DownloadData(createGroupURL);
            string webData = System.Text.Encoding.UTF8.GetString(raw);

            System.Net.WebClient wc1 = new System.Net.WebClient();
            byte[] raw1 = wc.DownloadData("http://launcher.zzl.org/getID.php");
            string webData1 = System.Text.Encoding.UTF8.GetString(raw1);

            ID = int.Parse(webData1.Split('\t')[0]);

            return parseAPIReturn(webData);

        }

        /// <summary>
        /// Adds an existing member to a group
        /// </summary>
        /// <param name="member">The members TOON name is a single string. Only group leader may add member to group.</param>
        /// <returns>Return an ArrayList as {1 for success / 0 for fail, Return Message}</returns>
        public ArrayList addMember(String member)
        {
            ArrayList retValue = new ArrayList();               // A retValue is used at the start and modified during the evalutaion. This allows
            // all return requirments to be met and returned at the end of method.

            if ((m_type != "") & (m_members_needed != 0))     // Make sure the group is not full
            {
                m_members_needed = m_members_needed - 1;        // Add 1 a member count to the group.

                String addMemberURL = "http://launcher.zzl.org/addMember.php?member=" + member + "&members_needed=" + m_members_needed + "&id=" + ID.ToString();     // Create a request URL

                System.Net.WebClient wc = new System.Net.WebClient();                   // Create webclient
                byte[] raw = wc.DownloadData(addMemberURL);                             // Setup a byte array to read the content displayed on the webpage.
                string webData = System.Text.Encoding.UTF8.GetString(raw);              // Convert the byte array into a usable string.

                return parseAPIReturn(webData);                                         // Parse the data from the API                   

            }

            if (m_members_needed == 0)
            {
                removeGroup();
            }

            

            return retValue;                                                            // Return retValue
        }

        /// <summary>
        /// Takes the response text and parses the CSV into an ArrayList
        /// </summary>
        /// <param name="responseText">Response text should be a CSV of the data requsted from API</param>
        /// <returns>Returns an ArrayList of the values</returns>
        private ArrayList parseAPIReturn(String responseText)
        {
            String[] responseData = responseText.Split(',');        // Split responseText by comma
            ArrayList returnValue = new ArrayList();                // Create return arraylist           

            foreach (String segment in responseData)                // Loop through each seperated value
            {
                returnValue.Add(segment.ToString());                // Add value the array list
            }

            return returnValue;                                     // Return the array list
        }

        /// <summary>
        /// Removes the group from the global list
        /// </summary>
        /// <returns>Returns an array list proccessed by parseAPIReturn</returns>
        public ArrayList removeGroup()
        {
            String removeGroup = "http://launcher.zzl.org/removeGroup.php?id=" + ID;
            System.Net.WebClient wc = new System.Net.WebClient();                   // Create webclient
            byte[] raw = wc.DownloadData(removeGroup);                              // Setup a byte array to read the content displayed on the webpage.
            string webData = System.Text.Encoding.UTF8.GetString(raw);              // Convert the byte array into a usable string.

            return parseAPIReturn(webData);                                         // Parse the data from the API   
        }

        public ArrayList getActiveGroups()
        {
            String removeGroup = "http://launcher.zzl.org/getGroups.php";

            System.Net.WebClient wc = new System.Net.WebClient();                   // Create webclient
            byte[] raw = wc.DownloadData(removeGroup);                              // Setup a byte array to read the content displayed on the webpage.
            string webData = System.Text.Encoding.UTF8.GetString(raw);              // Convert the byte array into a usable string.

            return parseAPIReturn(webData);                                         // Parse the data from the API   
        }

    }
}
