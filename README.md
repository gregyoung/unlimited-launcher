Unlimited Launcher
=================

Unlimited is a custom launcher in which can be used for user-friendly customization of the game _Toontown Infinite_.

### Contributing ###
Don't just start contributing. Discuss what you are about to do before you do something! **All** changes, except for emergency fixes, should be done _in_  a **seperate** branch **or** a fork -- **not** to the master branch. When your changes are ready to be merged, submit a _pull request_ for review.

**TODOs**

Logging into Game

Saving Usernames and Passwords

Saving Multiple Accounts

Run on Startup

Manual Updates

Automatic Updates

Improved User Interface

Easy Content Pack Loading

News Feed

Quick Links to Bug Reporters and IRC and other things

OS X and Linux Support

Launch All Servers from One Launcher

Faster and less buggy than current launchers

Switching graphics renderer
