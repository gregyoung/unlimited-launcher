﻿namespace Updater
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lbUpdatingFile = new System.Windows.Forms.Label();
            this.lbPercent = new System.Windows.Forms.Label();
            this.lbSpeed = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 29);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(306, 13);
            this.progressBar1.TabIndex = 1;
            // 
            // lbUpdatingFile
            // 
            this.lbUpdatingFile.AutoSize = true;
            this.lbUpdatingFile.Location = new System.Drawing.Point(12, 9);
            this.lbUpdatingFile.Name = "lbUpdatingFile";
            this.lbUpdatingFile.Size = new System.Drawing.Size(72, 13);
            this.lbUpdatingFile.TabIndex = 2;
            this.lbUpdatingFile.Text = "Updating File:";
            // 
            // lbPercent
            // 
            this.lbPercent.AutoSize = true;
            this.lbPercent.Location = new System.Drawing.Point(336, 29);
            this.lbPercent.Name = "lbPercent";
            this.lbPercent.Size = new System.Drawing.Size(21, 13);
            this.lbPercent.TabIndex = 3;
            this.lbPercent.Text = "0%";
            // 
            // lbSpeed
            // 
            this.lbSpeed.AutoSize = true;
            this.lbSpeed.Location = new System.Drawing.Point(138, 74);
            this.lbSpeed.Name = "lbSpeed";
            this.lbSpeed.Size = new System.Drawing.Size(38, 13);
            this.lbSpeed.TabIndex = 4;
            this.lbSpeed.Text = "Speed";
            // 
            // lbTotal
            // 
            this.lbTotal.AutoSize = true;
            this.lbTotal.Location = new System.Drawing.Point(138, 54);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(38, 13);
            this.lbTotal.TabIndex = 5;
            this.lbTotal.Text = "Speed";
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 96);
            this.Controls.Add(this.lbTotal);
            this.Controls.Add(this.lbSpeed);
            this.Controls.Add(this.lbPercent);
            this.Controls.Add(this.lbUpdatingFile);
            this.Controls.Add(this.progressBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "update";
            this.Text = "Unlimited Launcher Update";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lbUpdatingFile;
        private System.Windows.Forms.Label lbPercent;
        private System.Windows.Forms.Label lbSpeed;
        private System.Windows.Forms.Label lbTotal;
    }
}