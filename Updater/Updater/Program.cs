﻿using System;
using System.Threading;
using System.Net;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace Updater
{
    class Program
    {

        static bool finished_thread = false;

        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Console.SetWindowSize(100, 40);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("                                        Unlimited Launcher");
                getLauncherUpdates();
                Console.WriteLine("Choose Your Content Pack - ")
                    ;
                Console.WriteLine("\t0 - Original");
                Console.WriteLine("\t1 - Nightlife");
                Console.WriteLine("\t2 - Summer Pack");
                Console.Write("Choose: "); String type = Console.ReadLine(); int type_ = int.Parse(type);
                checkUpdates(type_);


                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "unlimited.exe";
                Process.Start(startInfo);
            }
            catch (Exception ex)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "unlimited.exe";
                Process.Start(startInfo);
            }

        }

        static private void appShortcutToDesktop(string linkName)
        {
            string deskDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

            using (StreamWriter writer = new StreamWriter(deskDir + "\\" + linkName + ".url"))
            {
                string app = System.Reflection.Assembly.GetExecutingAssembly().Location;
                writer.WriteLine("[InternetShortcut]");
                writer.WriteLine("URL=file:///" + app);
                writer.WriteLine("IconIndex=0");
                string icon = app.Replace('\\', '/');
                writer.WriteLine("IconFile=" + icon);
                writer.Flush();
            }
        }

        static public void getLauncherUpdates()
        {
            using (WebClient Client = new WebClient())
            {
                Console.WriteLine();
                Console.WriteLine();

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Updating/Checking Groups.dll");
                Client.DownloadFile("http://launcher.zzl.org/launcher/Groups.dll.dat", "Groups.dll");
                Console.WriteLine("Updating/Checking LaunchGame.dll");
                Client.DownloadFile("http://launcher.zzl.org/launcher/LaunchGame.dll.dat", "LaunchGame.dll");
                Console.WriteLine("Updating/Checking SavePassword.dll");
                Client.DownloadFile("http://launcher.zzl.org/launcher/SavePassword.dll.dat", "SavePassword.dll");
                Console.WriteLine("Updating/Checking ICSharpCode.SharpZipLib.dll");
                Client.DownloadFile("http://launcher.zzl.org/launcher/ICSharpCode.SharpZipLib.dll.dat", "ICSharpCode.SharpZipLib.dll");
                Console.WriteLine("Updating/Checking untar.dat");
                Client.DownloadFile("http://launcher.zzl.org/launcher/untar.exe.dat", "untar.exe");
                Console.WriteLine("Updating/Checking UnlimitedLauncher.dat");
                Client.DownloadFile("http://launcher.zzl.org/launcher/UnlimitedLauncher.exe.dat.zip", "unlimited.exe");
                System.Threading.Thread.Sleep(1500);
                Console.WriteLine();
                Console.WriteLine();
            }
        }

        static public void checkUpdates(int content_pack)
        {

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Toontown Infinite Details");

            XmlDocument document = new XmlDocument();

            if (content_pack == 0)
            {
                document.Load("http://download.toontowninfinite.com/test/win32/patcher.xml");
            }

            if (content_pack == 1)
            {
                document.Load("http://unlimitedlauncher.uhostall.com/patcher_night.xml");
            }

            if (content_pack == 2)
            {
                document.Load("http://unlimitedlauncher.uhostall.com/patcher_summer.xml");
            }

            XmlNodeList nl = document.SelectNodes("patcher");
            XmlNode root = nl[0];

            foreach (XmlNode xnode in root.ChildNodes)
            {
                string name = xnode.Name;

                try
                {
                    if (name == "launcher-version") { }
                    if (name == "account-server") { Console.WriteLine("Account Server: " + xnode.InnerText); }
                    if (name == "client-agent") { Console.WriteLine("Client Agent: " + xnode.InnerText); }
                    if (name == "server-version") { Console.WriteLine("Server Version: " + xnode.InnerText); }
                    if (name == "resources-revision") { Console.WriteLine("Resources Version: " + xnode.InnerText); }

                    if ((name == "directory"))
                    {
                        string directory = xnode.Attributes["name"].Value;

                        foreach (XmlNode reschildNode in xnode.ChildNodes)
                        {
                            string file = reschildNode.Attributes["name"].Value;
                            //string size = reschildNode.ChildNodes[0].InnerText;
                            string hash = "";
                            if (content_pack == 1)
                            {
                                hash = reschildNode.ChildNodes[0].InnerText;
                            }

                            if (content_pack == 0)
                            {
                                hash = reschildNode.ChildNodes[1].InnerText;
                            }

                            if (content_pack == 2)
                            {
                                hash = reschildNode.ChildNodes[0].InnerText;
                            }

                            if (directory == "")
                            {
                                if (getMD5(file) != hash)
                                { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine("Updating: " + file); getUpdates(file, directory, content_pack); Console.ForegroundColor = ConsoleColor.Red; }
                                else
                                { Console.WriteLine(file + " is up to date"); }
                            }
                            else
                            {
                                try
                                {
                                    try
                                    {
                                        if (getMD5(directory + @"\" + file) != hash)
                                        { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine("Updating: " + file); getUpdates(file, directory, content_pack); Console.ForegroundColor = ConsoleColor.Red; }
                                        else
                                        { Console.WriteLine(file + " is up to date"); }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine("Updating: " + file); getUpdates(file, directory, content_pack); Console.ForegroundColor = ConsoleColor.Red;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine("Updating: " + file); getUpdates(file, directory, content_pack); Console.ForegroundColor = ConsoleColor.Red;
                                }
                            }
                        }
                    }

                }
                catch (Exception ex) { Console.ForegroundColor = ConsoleColor.White; Console.WriteLine(ex); }

            }

        }

        static public void setDownloadFinished()
        {
            finished_thread = true;
        }

        static public void getUpdates(String file, String directory, int type)
        {
            if (directory == "")
            {
                try { File.Delete(file); }
                catch (Exception ex) { }

                WebClient wc = new WebClient();
                String link = "";
                if (type == 0)
                {
                    link = "http://download.toontowninfinite.com/test/win32/" + (string)file.Replace(file.Split('.')[1], "") + "bz2";
                }
                if (type == 1)
                {
                    link = "http://unlimitedlauncher.uhostall.com/" + (string)file.Replace(file.Split('.')[1], "") + "bz2";
                }
                if (type == 2)
                {
                    link = "http://unlimitedlauncher.uhostall.com/" + (string)file.Replace(file.Split('.')[1], "") + "bz2";
                }
                
                //String dir = file + ".bz2";

                Application.Run(new update(link, directory, file));
                //Thread thread = new Thread(() => downloadFile(link, directory));
                //thread.Start();

                while (finished_thread == false) { }
                finished_thread = false;

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "untar.exe";
                startInfo.Arguments = "-j " + file + ".bz2";
                Process p = Process.Start(startInfo);
                p.WaitForExit();

                System.Threading.Thread.Sleep(2000);

                File.Delete(file + ".bz2");

            }
            else
            {
                try { File.Delete(directory + @"\" + file); }
                catch (Exception ex) { }

                String link = "";
                if (type == 1)
                {
                    link = "http://unlimitedlauncher.uhostall.com/resources/" + (string)file.Replace(".mf", "") + ".bz2";             
                }

                if (type == 0)
                {
                    link = "http://download.toontowninfinite.com/test/win32/resources/" + (string)file.Replace(".mf", "") + ".bz2";
                }

                if (type == 2)
                {
                    link = "http://unlimitedlauncher.uhostall.com/summer/" + (string)file.Replace(".mf", "") + ".bz2";
                }

                //String dir = file + ".bz2";

                Application.Run(new update(link, directory, file));
                while (finished_thread == false) { }
                finished_thread = false;

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "untar.exe";
                startInfo.Arguments = "-j " + file + ".bz2";
                Process p = Process.Start(startInfo);
                p.WaitForExit();

                File.Move(file, directory + @"\" + file);

                File.Delete(file + ".bz2");
                
            }
        }

        static public String getMD5(String file)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(file))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }
            }
        }

    }
}
