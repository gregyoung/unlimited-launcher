﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace Updater
{
    public partial class update : Form
    {

        WebClient webClient;               // Our WebClient that will be doing the downloading for us
        Stopwatch sw = new Stopwatch();    // The stopwatch which we will be using to calculate the download speed

        String li = "";
        String dir = "";
        String file_name = "";

        public update()
        {
            InitializeComponent();
        }

        public update(String link, String directory, String file)
        {
            InitializeComponent();
            li = link;
            dir = directory;
            file_name = file;

            WebClient client = new WebClient();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);

            // Starts the download
            lbUpdatingFile.Text = "Updating File: " + file;
            client.DownloadFileAsync(new Uri(li), file_name + ".bz2", dir + @"\");
            //button1.Enabled = false;
            sw.Start();

        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            //MessageBox.Show(totalBytes.ToString());
            double percentage = bytesIn / totalBytes * 100;

            lbPercent.Text = e.ProgressPercentage.ToString() + "%";
            progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());
            

            lbTotal.Text = string.Format("{0} MB's / {1} MB's", (e.BytesReceived / 1024d / 1024d).ToString("0.00"), (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));
            lbSpeed.Text = string.Format("{0} kb/s", (e.BytesReceived / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00"));

        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Program.setDownloadFinished();
            this.Close();
            //MessageBox.Show("Download Completed");
            //button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WebClient client = new WebClient();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);

            // Starts the download
            client.DownloadFileAsync(new Uri(li), file_name + ".bz2", dir + @"\");
            //button1.Enabled = false;
        }

    }
}
