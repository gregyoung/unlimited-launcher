﻿namespace UnlimitedLauncher
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cbDistrict = new System.Windows.Forms.ComboBox();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.cbStreet = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(170, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Create Group";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 92);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(128, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "Members Needed";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(170, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Add Member";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(170, 70);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(97, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Close Group";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cbDistrict
            // 
            this.cbDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDistrict.FormattingEnabled = true;
            this.cbDistrict.Items.AddRange(new object[] {
            "Toontown Central",
            "-------------------",
            "Punchline Place",
            "Loopy Lane",
            "Silly Street",
            "------------------- ",
            "Donald\'s Dock",
            " ------------------",
            "Lighthouse Lane",
            "Barnacle Boulevard",
            "Seeweed Street",
            " ------------------",
            "Daisy\'s Garden",
            "------------------ ",
            "Oak Street",
            "Maple Street",
            "Elm Street",
            " ------------------ ",
            "Minnie\'s Melody Land",
            " ------------------ ",
            "Tennor Terrace",
            "Baritone Boulevard",
            "Alto Avenue",
            "------------------  ",
            "The Brrrgh",
            "------------------ ",
            "Polar Place",
            "Sleet Street",
            "Walrus Way",
            "------------------  ",
            "Donald\'s Dreamland",
            "------------------ ",
            "Lullaby Lane",
            "Pajama Place",
            "------------------ ",
            "COG HQ",
            "------------------ ",
            "Sellbot HQ",
            "Cashbot HQ",
            "Lawbot HQ",
            "Bossbot HQ"});
            this.cbDistrict.Location = new System.Drawing.Point(12, 12);
            this.cbDistrict.Name = "cbDistrict";
            this.cbDistrict.Size = new System.Drawing.Size(128, 21);
            this.cbDistrict.TabIndex = 7;
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "1 Story Bossbot",
            "2 Story Bossbot",
            "3 Story Bossbot",
            "4 Story Bossbot",
            "5 Story Bossbot",
            "",
            "1 Story Lawbot",
            "2 Story Lawbot",
            "3 Story Lawbot",
            "4 Story Lawbot",
            "5 Story Lawbot",
            "",
            "1 Story Cashbot",
            "2 Story Cashbot",
            "3 Story Cashbot",
            "4 Story Cashbot",
            "5 Story Cashbot",
            "",
            "1 Story Sellbot",
            "2 Story Sellbot",
            "3 Story Sellbot",
            "4 Story Sellbot",
            "5 Story Sellbot",
            "",
            "VP",
            "Front Factory",
            "Side Factory",
            "Long Factory",
            "",
            "CFO",
            "Coin Mint",
            "Dollar Mint",
            "Bullion Mint",
            "",
            "CJ",
            "DA Office A",
            "DA Office B",
            "DA Office C",
            "DA Office D",
            "",
            "CEO",
            "Front 3",
            "Middle 6",
            "Back 9",
            "",
            "Chairman"});
            this.cbType.Location = new System.Drawing.Point(12, 39);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(128, 21);
            this.cbType.TabIndex = 8;
            // 
            // cbStreet
            // 
            this.cbStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStreet.FormattingEnabled = true;
            this.cbStreet.Items.AddRange(new object[] {
            "Corky Bay",
            "Faircrest Farms",
            "Mild Meadows",
            "Roaring Rivers",
            "Silly Savannah",
            "Silly Slopes",
            "Wacky Woods",
            "Vintage Valley"});
            this.cbStreet.Location = new System.Drawing.Point(12, 66);
            this.cbStreet.Name = "cbStreet";
            this.cbStreet.Size = new System.Drawing.Size(128, 21);
            this.cbStreet.TabIndex = 9;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 129);
            this.Controls.Add(this.cbStreet);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.cbDistrict);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(312, 168);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(312, 168);
            this.Name = "Form2";
            this.Text = "Add Group";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cbDistrict;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.ComboBox cbStreet;
    }
}