﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using SavePassword;
using Microsoft.VisualBasic;
using LaunchGame;
using System.Security.Cryptography;
using Groups;


namespace UnlimitedLauncher
{
    public partial class Form1 : Form
    {
        bool boolUnlimited = false;
        static bool b = true;

        Groups.Group g;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

            Control.CheckForIllegalCrossThreadCalls = false;

            //System.Net.WebClient wcxx= new System.Net.WebClient();
            //wcxx.DownloadData("http://www.launcher.zzl.org/addCount.php");

            backgroundWorker2.RunWorkerAsync();

            System.Net.WebClient wcx = new System.Net.WebClient();
            wcx.DownloadData("http://www.launcher.zzl.org/addCount.php");

            try
            {
                comUser.Text = SavePass.getUser();
                txtPass.Text = SavePass.getPass();
            }
            catch (Exception ex)
            {

            }

            String md = "";

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead("Updater.exe"))
                {
                    md = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                }
            }

            System.Net.WebClient wc = new System.Net.WebClient();
            byte[] raw = wc.DownloadData("http://www.launcher.zzl.org/updater.txt");

            string hash = System.Text.Encoding.UTF8.GetString(raw);

            if (hash == md) { }
            else
            {
                try
                {
                    File.Delete("Updater.exe");
                }
                catch (Exception ex) { }



                using (WebClient Client = new WebClient())
                {
                    Client.DownloadFile("http://www.launcher.zzl.org/launcher/Updater.exe.dat", "Updater.exe");
                }

                MessageBox.Show("Unlimited Launcher has a critical launcher update and will update immediately.");
                Process.Start("Updater.exe");
                this.Close();

            }

            backgroundWorker1.RunWorkerAsync();
            Displaynotify();

        }

        protected void Displaynotify()
        {
            try
            {
                notifyIcon1.Icon = this.Icon;
                notifyIcon1.Text = "Unlimited Launcher";
                notifyIcon1.Visible = true;
                //notifyIcon1.BalloonTipTitle = "Welcome Devesh omar to Datatable Export Utlity";
                //notifyIcon1.BalloonTipText = "Click Here to see details";
                //notifyIcon1.ShowBalloonTip(100);
            }
            catch (Exception ex)
            {
            }
        }  

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            string strUser = comUser.Text;
            string strPass = txtPass.Text;
            bool boolSave = cbSave.Checked;
            if (e.KeyCode.Equals(Keys.Enter)) {
           PlayGame.LaunchGame(PlayGame.getToken(strUser, strPass));
                SavePass.save(strPass, strUser, boolSave);
                if (boolSave == true)
                {
                    Properties.Settings.Default.PassSaved = true;
                }
                Properties.Settings.Default.UserSaved = true;
            }
        }
        
       

        private void btnPlay_Click(object sender, EventArgs e)
        {
            string strUser = comUser.Text;
            string strPass = txtPass.Text;
            bool boolSave = cbSave.Checked;
            PlayGame.LaunchGame(PlayGame.getToken(strUser, strPass));
            SavePass.save(strPass, strUser, boolSave);
            if (boolSave == true)
            {
                Properties.Settings.Default.PassSaved = true;
            }
            Properties.Settings.Default.UserSaved = true;
        }

        //Ugh, sorry guys. Handles lblNeedAcc.Click
        private void label3_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.DefaultBroswer == true)
            {
                Process.Start("https://toontowninfinite.com/register/");
            }
            else
            {
                Form4 f = new Form4();
                f.navigate("https://toontowninfinite.com/register/");
                f.Show();
            }
        }

        private void btnCreateGroup_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //listView1.Items.Clear();
            //Groups.Group g = new Groups.Group();
            //ArrayList l = g.getActiveGroups();

            //for (int x = 0; x <= l.Count; x += 4)
            //{
             //   try
              //  {
              //      string[] row = { l[x].ToString(), l[x + 1].ToString(), l[x + 2].ToString(), l[x + 3].ToString() };
              //      var listViewItem = new ListViewItem(row);
              //      //listView1.Items.Add(listViewItem);
              //  }
              //  catch (Exception ex)
              //  {

               // }
            //}
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            ProcessStartInfo Info = new ProcessStartInfo();
            Info.Arguments = "/C choice /C Y /N /D Y /T 3 & Del " +
                           Application.ExecutablePath;
            Info.WindowStyle = ProcessWindowStyle.Hidden;
            Info.CreateNoWindow = true;
            Info.FileName = "cmd.exe";
            Process.Start(Info); 

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.DefaultBroswer == true)
            {
                Process.Start("https://bitbucket.org/gregyoung/unlimited-launcher/issues?status=new&status=open");
            }
            else
            {
                Form4 f = new Form4();
                f.navigate("https://bitbucket.org/gregyoung/unlimited-launcher/issues?status=new&status=open");
                f.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.DefaultBroswer == true)
            {
                Process.Start("https://toontowninfinite.com/toon-hq/");
            }
            else
            {
                Form4 f = new Form4();
                f.navigate("https://toontowninfinite.com/toon-hq/");
                f.Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.DefaultBroswer == true)
            {
                Process.Start("https://kiwiirc.com/client/irc.gamesurge.net/#tticommunity");
            }
            else
            {
                Form4 f = new Form4();
                f.navigate("https://kiwiirc.com/client/irc.gamesurge.net/#tticommunity");
                f.Show();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //you need to create an instanse of the object
            Form5 f = new Form5();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 f = new Form3();
            f.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AboutBox1 a = new AboutBox1();
            a.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //Make this label show on an error like "Your password or Username is incorrect." and "Toontown Infinite is under Maintenance." etc...
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {
            //Options options = new Options();
            //options.Show();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            ArrayList notifications = new ArrayList(); ;
            notifications.Add("");
            while (b)
            {
                Groups.Group gg = new Groups.Group();
                ArrayList ll = gg.getActiveGroups();

                listView1.Items.Clear();
                for (int p = 0; p <= ll.Count; p += 5)
                {
                    try
                    {
                        bool add = false;
                        string[] row = { ll[p + 2].ToString(), ll[p + 1].ToString(), ll[p].ToString(), ll[p + 3].ToString() };
                        String tmp_note = ll[p].ToString() + "," + ll[p + 1].ToString() + "," + ll[p + 2].ToString() + "," + ll[p + 4].ToString();      
                        var listViewItem = new ListViewItem(row);
                        listView1.Items.Add(listViewItem);

                        bool display = true;
                        foreach (String note in notifications)
                        {
                            if (tmp_note == note)
                            {
                                display = false;
                            }
                        }

                        if (display)
                        {
                            notifications.Add(tmp_note);
                            notifyIcon1.BalloonTipTitle = "New Group";
                            notifyIcon1.BalloonTipText = "Street: " + ll[p].ToString() + Environment.NewLine + "Type: " + ll[p + 1].ToString() + Environment.NewLine +
                                "District: " + ll[p + 2].ToString();
                            notifyIcon1.ShowBalloonTip(100);
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }



                System.Threading.Thread.Sleep(1000);

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            b = false;

            System.Net.WebClient wc = new System.Net.WebClient();
            wc.DownloadData("http://www.launcher.zzl.org/removeCount.php");

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Form5 f = new Form5();
            f.ShowDialog() ;

            ArrayList member_toons = new ArrayList();
            //g = new Groups.Group("n/A", f.type, "n/a", f.disctict, int.Parse(f.toons), member_toons, f.street);


            try
            {
                g = new Groups.Group(f.type, f.street, int.Parse(f.toons), f.disctict);
            }
            catch
            {

            }
          
           

            button9.Enabled = false;
            button8.Enabled = true;
            button6.Enabled = true;

        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                g.addMember("ABC");
            }
            catch
            {
                button8.Enabled = false;
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            try
            {
                g.removeGroup();
                button9.Enabled = true
                    ;
            }
            finally
            {
                button6.Enabled = false;
                button8.Enabled = false;
            }
            
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            while (b)
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                byte[] raw = wc.DownloadData("http://www.launcher.zzl.org/getCount.php");
                label1.Text = "Unlimited Launcher Users Online: " + System.Text.Encoding.UTF8.GetString(raw);
                System.Threading.Thread.Sleep(1000);
            }
        }

        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void btnUIRC_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.DefaultBroswer == true)
            {
                Process.Start("https://kiwiirc.com/client/irc.gamesurge.net/#unlimitedlauncher");
            }
            else
            {
                Form4 f = new Form4();
                f.navigate("https://kiwiirc.com/client/irc.gamesurge.net/#unlimitedlauncher");
                f.Show();
            }
        }

        private void btnSwapNews_Click(object sender, EventArgs e)
        {
           
            if (boolUnlimited == false)
            {
                webNews.Navigate("http://unlimitedlauncher.wordpress.com/");
                btnSwapNews.Text = "Infinite News";
                boolUnlimited = true;
            }
            else
            {
                webNews.Navigate("https://toontowninfinite.com/api/latestPost/?id=25");
                btnSwapNews.Text = "Unlimited News";
                boolUnlimited = false;
            }
        }

    }
}