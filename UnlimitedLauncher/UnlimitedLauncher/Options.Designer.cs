﻿namespace UnlimitedLauncher
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcOptions = new System.Windows.Forms.TabControl();
            this.tabLauncher = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblDirectoryPrompt = new System.Windows.Forms.Label();
            this.radDefault = new System.Windows.Forms.RadioButton();
            this.radUnlimited = new System.Windows.Forms.RadioButton();
            this.lblLinksTitle = new System.Windows.Forms.Label();
            this.tabGraphics = new System.Windows.Forms.TabPage();
            this.gbRenderer = new System.Windows.Forms.GroupBox();
            this.radDirectX = new System.Windows.Forms.RadioButton();
            this.radOpenGL = new System.Windows.Forms.RadioButton();
            this.lblRendererTitle = new System.Windows.Forms.Label();
            this.txtDirectory = new System.Windows.Forms.TextBox();
            this.fileLocation = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.tbcOptions.SuspendLayout();
            this.tabLauncher.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabGraphics.SuspendLayout();
            this.gbRenderer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbcOptions
            // 
            this.tbcOptions.Controls.Add(this.tabLauncher);
            this.tbcOptions.Controls.Add(this.tabGraphics);
            this.tbcOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcOptions.Location = new System.Drawing.Point(0, 0);
            this.tbcOptions.Name = "tbcOptions";
            this.tbcOptions.SelectedIndex = 0;
            this.tbcOptions.Size = new System.Drawing.Size(372, 313);
            this.tbcOptions.TabIndex = 15;
            // 
            // tabLauncher
            // 
            this.tabLauncher.Controls.Add(this.groupBox2);
            this.tabLauncher.Controls.Add(this.groupBox1);
            this.tabLauncher.Location = new System.Drawing.Point(4, 22);
            this.tabLauncher.Name = "tabLauncher";
            this.tabLauncher.Size = new System.Drawing.Size(364, 287);
            this.tabLauncher.TabIndex = 2;
            this.tabLauncher.Text = "Launcher";
            this.tabLauncher.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radDefault);
            this.groupBox1.Controls.Add(this.radUnlimited);
            this.groupBox1.Controls.Add(this.lblLinksTitle);
            this.groupBox1.Location = new System.Drawing.Point(9, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(347, 95);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtDirectory);
            this.groupBox2.Controls.Add(this.lblDirectoryPrompt);
            this.groupBox2.Location = new System.Drawing.Point(8, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(347, 101);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // lblDirectoryPrompt
            // 
            this.lblDirectoryPrompt.AutoSize = true;
            this.lblDirectoryPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirectoryPrompt.Location = new System.Drawing.Point(6, 16);
            this.lblDirectoryPrompt.Name = "lblDirectoryPrompt";
            this.lblDirectoryPrompt.Size = new System.Drawing.Size(104, 17);
            this.lblDirectoryPrompt.TabIndex = 1;
            this.lblDirectoryPrompt.Text = "Game Location";
            // 
            // radDefault
            // 
            this.radDefault.AutoSize = true;
            this.radDefault.Checked = true;
            this.radDefault.Location = new System.Drawing.Point(6, 36);
            this.radDefault.Name = "radDefault";
            this.radDefault.Size = new System.Drawing.Size(100, 17);
            this.radDefault.TabIndex = 3;
            this.radDefault.TabStop = true;
            this.radDefault.Text = "Default Browser";
            this.radDefault.UseVisualStyleBackColor = true;
            this.radDefault.CheckedChanged += new System.EventHandler(this.radDefault_CheckedChanged);
            this.radDefault.Click += new System.EventHandler(this.radDefault_Click);
            // 
            // radUnlimited
            // 
            this.radUnlimited.AutoSize = true;
            this.radUnlimited.Location = new System.Drawing.Point(6, 59);
            this.radUnlimited.Name = "radUnlimited";
            this.radUnlimited.Size = new System.Drawing.Size(109, 17);
            this.radUnlimited.TabIndex = 2;
            this.radUnlimited.Text = "Unlimited Browser";
            this.radUnlimited.UseVisualStyleBackColor = true;
            this.radUnlimited.CheckedChanged += new System.EventHandler(this.radUnlimited_CheckedChanged);
            this.radUnlimited.Click += new System.EventHandler(this.radUnlimited_Click);
            // 
            // lblLinksTitle
            // 
            this.lblLinksTitle.AutoSize = true;
            this.lblLinksTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinksTitle.Location = new System.Drawing.Point(6, 16);
            this.lblLinksTitle.Name = "lblLinksTitle";
            this.lblLinksTitle.Size = new System.Drawing.Size(41, 17);
            this.lblLinksTitle.TabIndex = 1;
            this.lblLinksTitle.Text = "Links";
            // 
            // tabGraphics
            // 
            this.tabGraphics.Controls.Add(this.gbRenderer);
            this.tabGraphics.Location = new System.Drawing.Point(4, 22);
            this.tabGraphics.Name = "tabGraphics";
            this.tabGraphics.Padding = new System.Windows.Forms.Padding(3);
            this.tabGraphics.Size = new System.Drawing.Size(364, 287);
            this.tabGraphics.TabIndex = 1;
            this.tabGraphics.Text = "Graphics";
            this.tabGraphics.UseVisualStyleBackColor = true;
            // 
            // gbRenderer
            // 
            this.gbRenderer.Controls.Add(this.radDirectX);
            this.gbRenderer.Controls.Add(this.radOpenGL);
            this.gbRenderer.Controls.Add(this.lblRendererTitle);
            this.gbRenderer.Location = new System.Drawing.Point(9, 7);
            this.gbRenderer.Name = "gbRenderer";
            this.gbRenderer.Size = new System.Drawing.Size(349, 93);
            this.gbRenderer.TabIndex = 0;
            this.gbRenderer.TabStop = false;
            // 
            // radDirectX
            // 
            this.radDirectX.AutoSize = true;
            this.radDirectX.Enabled = false;
            this.radDirectX.Location = new System.Drawing.Point(9, 60);
            this.radDirectX.Name = "radDirectX";
            this.radDirectX.Size = new System.Drawing.Size(172, 17);
            this.radDirectX.TabIndex = 2;
            this.radDirectX.TabStop = true;
            this.radDirectX.Text = "DirectX (Faster, More Crashing)";
            this.radDirectX.UseVisualStyleBackColor = true;
            // 
            // radOpenGL
            // 
            this.radOpenGL.AutoSize = true;
            this.radOpenGL.Enabled = false;
            this.radOpenGL.Location = new System.Drawing.Point(9, 37);
            this.radOpenGL.Name = "radOpenGL";
            this.radOpenGL.Size = new System.Drawing.Size(178, 17);
            this.radOpenGL.TabIndex = 1;
            this.radOpenGL.TabStop = true;
            this.radOpenGL.Text = "OpenGL (Slower, Less Crashing)";
            this.radOpenGL.UseVisualStyleBackColor = true;
            // 
            // lblRendererTitle
            // 
            this.lblRendererTitle.AutoSize = true;
            this.lblRendererTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRendererTitle.Location = new System.Drawing.Point(6, 16);
            this.lblRendererTitle.Name = "lblRendererTitle";
            this.lblRendererTitle.Size = new System.Drawing.Size(129, 17);
            this.lblRendererTitle.TabIndex = 0;
            this.lblRendererTitle.Text = "Graphics Renderer";
            // 
            // txtDirectory
            // 
            this.txtDirectory.Location = new System.Drawing.Point(9, 40);
            this.txtDirectory.Name = "txtDirectory";
            this.txtDirectory.Size = new System.Drawing.Size(332, 20);
            this.txtDirectory.TabIndex = 3;
            this.txtDirectory.Text = "C:\\\\Program Files (x86)\\\\Toontown Infinite\\\\Test";
            this.txtDirectory.TextChanged += new System.EventHandler(this.txtDirectory_TextChanged);
            // 
            // fileLocation
            // 
            this.fileLocation.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-3, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(352, 33);
            this.label1.TabIndex = 4;
            this.label1.Text = "Please use two backslashes instead of one for every backslash necessary to preven" +
    "t crashing. This will not be necessary in a future update.";
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 313);
            this.Controls.Add(this.tbcOptions);
            this.Name = "Options";
            this.Text = "Options";
            this.Load += new System.EventHandler(this.Options_Load);
            this.tbcOptions.ResumeLayout(false);
            this.tabLauncher.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabGraphics.ResumeLayout(false);
            this.gbRenderer.ResumeLayout(false);
            this.gbRenderer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbcOptions;
        private System.Windows.Forms.TabPage tabGraphics;
        private System.Windows.Forms.TabPage tabLauncher;
        private System.Windows.Forms.GroupBox gbRenderer;
        private System.Windows.Forms.RadioButton radDirectX;
        private System.Windows.Forms.RadioButton radOpenGL;
        private System.Windows.Forms.Label lblRendererTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblLinksTitle;
        private System.Windows.Forms.RadioButton radDefault;
        private System.Windows.Forms.RadioButton radUnlimited;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDirectoryPrompt;
        private System.Windows.Forms.TextBox txtDirectory;
        private System.Windows.Forms.OpenFileDialog fileLocation;
        private System.Windows.Forms.Label label1;
    }
}