﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnlimitedLauncher
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }

        private void radDefault_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radUnlimited_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Options_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.DefaultBroswer == false)
            {
                radDefault.Checked = false;
                radUnlimited.Checked = true;
            }
            else
            {
                radDefault.Checked = true;
            }
        }

        private void radDefault_Click(object sender, EventArgs e)
        {
            if (radDefault.Checked == true)
            {
                Properties.Settings.Default.DefaultBroswer = true;
            }
        }

        private void radUnlimited_Click(object sender, EventArgs e)
        {
            if (radDefault.Checked == false)
            {
                Properties.Settings.Default.DefaultBroswer = false;
            }
        }

        private void txtDirectory_TextChanged(object sender, EventArgs e)
        {
            string fileLoc = txtDirectory.Text;
            fileLoc = fileLoc + "\\";
           Properties.Settings.Default.gameLoc = fileLoc;
            
        }
    }
}