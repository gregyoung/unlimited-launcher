﻿namespace UnlimitedLauncher
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
            this.cbStreet = new System.Windows.Forms.ComboBox();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.cbDistrict = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.cbMembersNeeded = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbStreet
            // 
            this.cbStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStreet.FormattingEnabled = true;
            this.cbStreet.Items.AddRange(new object[] {
            "Corky Bay",
            "Faircrest Farms",
            "Mild Meadows",
            "Roaring Rivers",
            "Silly Savannah",
            "Silly Slopes",
            "Wacky Woods",
            "Vintage Valley"});
            this.cbStreet.Location = new System.Drawing.Point(109, 69);
            this.cbStreet.Name = "cbStreet";
            this.cbStreet.Size = new System.Drawing.Size(128, 21);
            this.cbStreet.TabIndex = 22;
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "1 Story Bossbot",
            "2 Story Bossbot",
            "3 Story Bossbot",
            "4 Story Bossbot",
            "5 Story Bossbot",
            "",
            "1 Story Lawbot",
            "2 Story Lawbot",
            "3 Story Lawbot",
            "4 Story Lawbot",
            "5 Story Lawbot",
            "",
            "1 Story Cashbot",
            "2 Story Cashbot",
            "3 Story Cashbot",
            "4 Story Cashbot",
            "5 Story Cashbot",
            "",
            "1 Story Sellbot",
            "2 Story Sellbot",
            "3 Story Sellbot",
            "4 Story Sellbot",
            "5 Story Sellbot",
            "",
            "VP",
            "Front Factory",
            "Side Factory",
            "Long Factory",
            "",
            "CFO",
            "Coin Mint",
            "Dollar Mint",
            "Bullion Mint",
            "",
            "CJ",
            "DA Office A",
            "DA Office B",
            "DA Office C",
            "DA Office D",
            "",
            "CEO",
            "Front 3",
            "Middle 6",
            "Back 9"});
            this.cbType.Location = new System.Drawing.Point(109, 42);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(128, 21);
            this.cbType.TabIndex = 21;
            // 
            // cbDistrict
            // 
            this.cbDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDistrict.FormattingEnabled = true;
            this.cbDistrict.Items.AddRange(new object[] {
            "Punchline Place",
            "Loopy Lane",
            "Silly Street",
            "------------------- ",
            "Donald\'s Dock",
            " ------------------",
            "Lighthouse Lane",
            "Barnacle Boulevard",
            "Seeweed Street",
            " ------------------",
            "Daisy\'s Garden",
            "------------------ ",
            "Oak Street",
            "Maple Street",
            "Elm Street",
            " ------------------ ",
            "Minnie\'s Melody Land",
            " ------------------ ",
            "Tennor Terrace",
            "Baritone Boulevard",
            "Alto Avenue",
            "------------------  ",
            "The Brrrgh",
            "------------------ ",
            "Polar Place",
            "Sleet Street",
            "Walrus Way",
            "------------------  ",
            "Donald\'s Dreamland",
            "------------------ ",
            "Lullaby Lane",
            "Pajama Place",
            "------------------ ",
            "COG HQ",
            "------------------ ",
            "Sellbot HQ",
            "Cashbot HQ",
            "Lawbot HQ",
            "Bossbot HQ"});
            this.cbDistrict.Location = new System.Drawing.Point(109, 15);
            this.cbDistrict.Name = "cbDistrict";
            this.cbDistrict.Size = new System.Drawing.Size(128, 21);
            this.cbDistrict.TabIndex = 20;
            // 
            // cbMembersNeeded
            // 
            this.cbMembersNeeded.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMembersNeeded.FormattingEnabled = true;
            this.cbMembersNeeded.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.cbMembersNeeded.Location = new System.Drawing.Point(109, 93);
            this.cbMembersNeeded.Name = "cbMembersNeeded";
            this.cbMembersNeeded.Size = new System.Drawing.Size(128, 21);
            this.cbMembersNeeded.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "District:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Location:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Members Needed";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(251, 109);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 144);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbMembersNeeded);
            this.Controls.Add(this.cbStreet);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.cbDistrict);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Groups";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbStreet;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.ComboBox cbDistrict;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox cbMembersNeeded;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;

    }
}