﻿namespace UnlimitedLauncher
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.gbContentPacks = new System.Windows.Forms.GroupBox();
            this.radNightLifeCP = new System.Windows.Forms.RadioButton();
            this.radOriginalCP = new System.Windows.Forms.RadioButton();
            this.lblCP = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.gbContentPacks.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbContentPacks
            // 
            this.gbContentPacks.Controls.Add(this.radNightLifeCP);
            this.gbContentPacks.Controls.Add(this.radOriginalCP);
            this.gbContentPacks.Controls.Add(this.lblCP);
            this.gbContentPacks.Location = new System.Drawing.Point(12, 12);
            this.gbContentPacks.Name = "gbContentPacks";
            this.gbContentPacks.Size = new System.Drawing.Size(308, 125);
            this.gbContentPacks.TabIndex = 9;
            this.gbContentPacks.TabStop = false;
            // 
            // radNightLifeCP
            // 
            this.radNightLifeCP.AutoSize = true;
            this.radNightLifeCP.Enabled = false;
            this.radNightLifeCP.Location = new System.Drawing.Point(6, 67);
            this.radNightLifeCP.Name = "radNightLifeCP";
            this.radNightLifeCP.Size = new System.Drawing.Size(70, 17);
            this.radNightLifeCP.TabIndex = 14;
            this.radNightLifeCP.TabStop = true;
            this.radNightLifeCP.Text = "Night Life";
            this.radNightLifeCP.UseVisualStyleBackColor = true;
            // 
            // radOriginalCP
            // 
            this.radOriginalCP.AutoSize = true;
            this.radOriginalCP.Checked = true;
            this.radOriginalCP.Location = new System.Drawing.Point(7, 44);
            this.radOriginalCP.Name = "radOriginalCP";
            this.radOriginalCP.Size = new System.Drawing.Size(60, 17);
            this.radOriginalCP.TabIndex = 13;
            this.radOriginalCP.TabStop = true;
            this.radOriginalCP.Text = "Original";
            this.radOriginalCP.UseVisualStyleBackColor = true;
            // 
            // lblCP
            // 
            this.lblCP.AutoSize = true;
            this.lblCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCP.Location = new System.Drawing.Point(6, 16);
            this.lblCP.Name = "lblCP";
            this.lblCP.Size = new System.Drawing.Size(130, 24);
            this.lblCP.TabIndex = 12;
            this.lblCP.Text = "Content Packs";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(128, 143);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 178);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gbContentPacks);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(350, 217);
            this.MinimumSize = new System.Drawing.Size(350, 217);
            this.Name = "Form3";
            this.Text = "Content Packs";
            this.gbContentPacks.ResumeLayout(false);
            this.gbContentPacks.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbContentPacks;
        private System.Windows.Forms.RadioButton radNightLifeCP;
        private System.Windows.Forms.RadioButton radOriginalCP;
        private System.Windows.Forms.Label lblCP;
        private System.Windows.Forms.Button button1;
    }
}